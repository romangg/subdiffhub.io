---
layout: page
title: About me
permalink: /about/
banner_image: about-me-banner.jpg
banner_image_alt: About me
---

Hi, thank you for visiting my incredible small yet somewhat ambitious blog venture.

My name is Roman Gilg. I'm from Germany and I'm interested in many different things. Some of them I hope to put up on this blog.

My current life goals: For the next few months I'll primarily write on my master thesis in mathematics. Besides that I'm trying to get better at programming as a newcomer in practical terms.

I do some work for the [KDE project][kde] in order to achieve this.

### Contact me

Find me on [Twitter][Twitter] / [Github][github] or just say `Hello` at [subdiff@gmail.com](subdiff@gmail.com).

[kde]: https://www.kde.org/
[twitter]: https://twitter.com/subdiff
[github]: https://github.com/subdiff
